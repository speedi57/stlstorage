#!/usr/bin/env python
# -*- coding:utf-8 -*-
try:
    from PIL import Image
except ImportError:
    import Image
import os, urllib
from django.core.servers.basehttp import FileWrapper
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
category = ["Art","Fashion","Gadgets","Hobby","Toys & Games","Tools","Models"]
def extension_ok(nomfic):
    """ Renvoie True si le fichier possede une extension d'image valide. """
    return '.' in nomfic and nomfic.rsplit('.', 1)[1] in ('stl')
def extension_ok2(nomfic):
    """ Renvoie True si le fichier possede une extension d'image valide. """
    return '.' in nomfic and nomfic.rsplit('.', 1)[1] in ('png')
def resize(file):
    return Image.open(file).resize((300, 300), Image.ANTIALIAS)
class HttpResponseSendfile(HttpResponse):
    """
    HttpResponse for using X-Sendfile.
  
    Uses FileWrapper as a fallback if the front-end server doesn't intercept
    X-Sendfile headers.
    """
    def __init__(self, path, mimetype=None, content_type=None, fallback=True):
        exists = os.path.exists(path)
        status = 200 if exists else 404
    
        # Fallback for lack of X-Sendfile support
        content = ''
        if exists and fallback:
            content = FileWrapper(open(path, 'rb'))
    
        # Common elements
        super(HttpResponseSendfile, self).__init__(content, mimetype, status, content_type)
        self['Content-Length'] = os.path.getsize(path) if exists else 0
        filename = urllib.quote(os.path.basename(path).encode('utf8'))
        self['Content-Disposition'] = 'attachment; filename="%s"' % filename
    
        # X-Sendfile
        self['X-Sendfile'] = path
def  pagination (data,NbPage = 1):
    paginator = Paginator(data, 5)
    try:
        contacts = paginator.page(NbPage)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        contacts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        contacts = paginator.page(paginator.num_pages)
    return contacts