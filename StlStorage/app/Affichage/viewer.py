from django.contrib import messages
from django.core.files.base import ContentFile
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.shortcuts import render
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.core.files.storage import default_storage
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse
from django.core.servers.basehttp import FileWrapper
from django.template import RequestContext
from datetime import datetime
from django.contrib import messages
from ..models import *
from ..forms import *
from ..fct import *
from werkzeug import secure_filename
import os,sys,struct, base64, time, uuid , shutil,  tempfile, zipfile
from datetime import datetime 
from scipy.misc import imread
from stl_tools import numpy2stl
from scipy.misc import imresize
from scipy.ndimage import gaussian_filter
import os
import mimetypes
from django.http import StreamingHttpResponse
try:
    from PIL import Image
except ImportError:
    import Image
def view3d(request,  code, createBy):
    """
    get file from serveur and send to broswer
    """
    if request.method == 'POST':
        username = request.user.username
        titre = request.POST['Commentaire']
        a1 = File.objects.get(title=str(code),user=str(createBy))
        a1.Com.create(UserCom = username , Text = titre)
        a1.save()
    #nom = nom.replace('.png','')
    if True :#os.path.isfile(dossier_object + nom +'.stl'): # si le fichier existe on l'envoie
        desc = list(File.objects.filter(title=str(code),user=str(createBy)).values())[0]
        com = File.objects.get(title=str(code),user=str(createBy))
        List =  list(com.Com.all().values())
        return render(
        request,
        '3dview.html',
        context_instance = RequestContext(request,
        {
            'title':'3D Viewer',
            'year':datetime.now().year,
            'description': desc,
            'Commentaire':List,
        })
    )

def view(request, NbPage=1):
    """Renders the view page.
    list of 3d page and possibility to tree by categorie
    """
    assert isinstance(request, HttpRequest)
    nbelement = 10
    if request.method == 'POST':
        cate= request.POST['categorie']
        if cate != "*":
            data = list(File.objects.filter(Categorie=cate).values())
            paginator = Paginator(data, nbelement)
            try:
                contacts = paginator.page(NbPage)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                contacts = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                contacts = paginator.page(paginator.num_pages)
            data = contacts
        else:
            data = list(File.objects.values())
            paginator = Paginator(data, nbelement)
            try:
                contacts = paginator.page(NbPage)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                contacts = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                contacts = paginator.page(paginator.num_pages)
            data = contacts
    else:
        data = list(File.objects.values())
        paginator = Paginator(data, nbelement)
        try:
            contacts = paginator.page(NbPage)
        except PageNotAnInteger:
             # If page is not an integer, deliver first page.
            contacts = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            contacts = paginator.page(paginator.num_pages)
        data = contacts
    return render(
            request,
            'list.html',
            context_instance = RequestContext(request,
            {
                'title':'List File',
                'data':data,
                'cat':category,
                'year':datetime.now().year,
                'nb':paginator.page_range,
            })
        )

def User(request,createBy ):
    """Renders the view page.
    list of 3d page and possibility to tree by categorie
    """
    assert isinstance(request, HttpRequest)
    if request.method == 'POST':
        cate= request.POST['categorie']
        if cate != "*":
            data = list(File.objects.filter(Categorie=cate,user=str(createBy)).values())
        else:
            data = list(File.objects.filter(user=str(createBy)).values())
    else:
        data = list(File.objects.filter(user=str(createBy)).values())
    return render(
            request,
            'list.html',
            context_instance = RequestContext(request,
            {
                'title':'List File',
                'data':data,
                'cat':category,
                'year':datetime.now().year,
            })
        )

def ImageToStl(request,):
    """
    Convertion d'image vers fichier stl imprimable
    """
    if request.method == 'POST':
        f = request.FILES['fileToUpload']
        folder = "../StlStorage2/app/static/app/temp/" + str(f)
        default_storage.save( folder, ContentFile(f.read()))
        im = Image.open(folder)
        im.save(folder+'.jpg', "JPEG")
        folder = folder+'.jpg'
        A =imread(folder)
        A = A[:, :, 2] + 1.0*A[:,:, 0] # Compose RGBA channels to give depth
        A = gaussian_filter(A, 1)
        numpy2stl(A, folder+".stl", scale=0.05, mask_val=1., solid=True)
        os.remove(folder)
        return HttpResponseRedirect('/static/app/temp/'+str(f)+'.jpg.stl')
    else :
        return render(
            request,
            'ImageToStl.html',
            context_instance = RequestContext(request,
            {
                'title':'Image To Stl',
                'year':datetime.now().year,
            })
        )