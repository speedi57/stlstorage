﻿//Code....

window.onload = init; //SANS parenthèses (pas le droit aux arguments donc :( )
function init() {
    $("#Image").hide();
    $("#Desc").hide();
}
function fileSelected() {
    var file = document.getElementById('fileToUpload').files[0];
    if (file) {
        var fileSize = 0;
        if (file.size > 1024 * 1024)
            fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
        else
            fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';

        document.getElementById('fileName').innerHTML = 'Name: ' + file.name;
        document.getElementById('fileSize').innerHTML = 'Size: ' + fileSize;
        valid();
    }
}
function valid() {
    var file_name = document.getElementById('fileToUpload').files[0].name;
    var file_array = file_name.split(".");
    var file_array1 = file_array[1].toLowerCase();
    if (file_array1 == 'stl') {
        $("#Image").show();
        return true;
    }
    else {
        alert("Please Upload Valid 3D file");
        return false;
    }
}
function validImage() {
    var file_name = document.getElementById('ImToUpload').files[0].name;
    var file_array = file_name.split(".");
    var file_array1 = file_array[1].toLowerCase();
    if (file_array1 == 'png' || file_array1 == 'jpg' || file_array1 == 'gif' || file_array1 == 'tiff') {
        $("#Desc").show();
        return true;
    }
    else {
        alert("Please Upload Valid Image file");
        return false;
    }
}