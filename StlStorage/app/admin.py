from django.contrib import admin
from .models import File , Commentaire

admin.site.register(File)
admin.site.register(Commentaire)
class AuthorAdmin(admin.ModelAdmin):
    pass