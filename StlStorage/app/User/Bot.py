import os
class IA:
    def init(self):
        import aiml
        # The Kernel object is the public interface to
        # the AIML interpreter.
        self.k = aiml.Kernel()
        # Use the 'learn' method to load the contents
        # of an AIML file into the Kernel.
        current = os.getcwd()
        if os.name == 'nt' :
            os.chdir("E:/projet/programme/python/stl-storage/StlStorage2/app/")
            self.k.learn("std-startup.xml")
        else :
            try:
                os.chdir("/var/www/python/app/User")
            except :
                pass
            self.k.learn("linux.xml")
        # Use the 'respond' method to compute the response
        # to a user's input string.  respond() returns
        # the interpreter's response, which in this case
        # we ignore.
        self.k.setBotPredicate("name", "Alice")
        self.k.setBotPredicate("botmaster", "Boss")
        self.k.setBotPredicate("email", "postmaster@warsearth.fr")
        self.k.setBotPredicate("birthplace", "19/08/1993")
        self.k.setBotPredicate("gender", "girl")
        self.k.respond('LOAD AIML B')
        os.chdir = current
    def Reponse(self,Message):
        return  self.k.respond(Message)