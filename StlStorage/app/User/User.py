#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.contrib import messages
from django.core.files.base import ContentFile
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.shortcuts import render
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.core.files.storage import default_storage
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse
from django.core.servers.basehttp import FileWrapper
from django.template import RequestContext
from datetime import datetime
from django.contrib import messages
from ..models import *
from ..forms import *
from ..fct import *
from werkzeug import secure_filename
import os,sys
from datetime import datetime 
from .Bot import IA
IA_support = IA()
def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'contact.html',
        context_instance = RequestContext(request,
        {
            'title':'Contact',
            'year':datetime.now().year,
        })
    )

def register(request):
    """
    view to :
    register user 
    """
    if request.method == 'POST':
        form = UserCreateForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            return HttpResponseRedirect("/")
        else :
            messages.warning(request, 'Info is not corrext')
            return render(
                request,
                'register.html',
                context_instance = RequestContext(request,
                {
                    'messages': "Data Error",
                    'title':'Register',
			        'form': form,
                    'year':datetime.now().year,
                })
            )
    else:
        form = UserCreateForm()
        return render(
                request,
                'register.html',
                context_instance = RequestContext(request,
                {
                    'title':'Register',
			        'form': form,
                    'year':datetime.now().year,
                })
            )

def upload(request):
    """
    first part of uplad
    Send stl to serveur
    """
    if request.method == 'POST':
        print request.POST
        f = request.FILES['fileToUpload']
        Photo = request.FILES['ImToUpload']
        if f: # on verifie qu'un fichier a bien ete envoye
            if True: #extension_ok(f):  on verifie que son extension est valide 
                temp = str(uuid.uuid4()).replace("-", "")+".stl"#generation d'un nom unique 0
                nom = secure_filename(temp)
                default_storage.save("../StlStorage2/app/static/app/objects/" + nom, ContentFile(f.read()))
                test = nom.replace('.stl','')
                resize(Photo).save("../StlStorage2/app/static/app/objects/" + test +'.png')
                titre = request.POST['Titre']
                description = request.POST['Fname']
                cate= request.POST['categorie']
                if description:
                    data =File(title=titre,NameFile =test,Description =description,Categorie =cate,user=request.user.username)
                    data.save()
                return redirect('/')
            else:
                messages.error(request, 'This file does not have an authorized extension !')
        else:
             messages.error(request, 'You have forget the file !')
    form = UploadFile()
    return render(
                    request,
                    'up.html',
                     context_instance = RequestContext(request,
                     {
                        'title':'Upload',
                        'cat':category,
                        'year':datetime.now().year,
                        'form' : form,
                      })
                    )

def support(request):
    """                                                                         
    interface for use pyaiml at virtual support                                             
    """
    demande =''
    if request.method == 'POST':
        if  request.POST['init'] != '':
            try :
                IA_support.init()
            except :
                pass
            demande = 'OK'
        if request.POST['message'] != '':
            demande = IA_support.Reponse(request.POST['message'])
            demande = '<li> Alice :' + demande+'</li>'
        return HttpResponse(demande)
    return render(
        request,
        'support.html',
        context_instance = RequestContext(request,
        {
            'title':'support',
            'year':datetime.now().year,
        })
    )