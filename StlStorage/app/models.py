"""
Definition of models.
"""

from django.db import models
# Create your models here.
class Commentaire(models.Model):
    UserCom = models.CharField(max_length=30)
    Text = models.CharField(max_length=300)
class File(models.Model):
    """
    Model for stl file
    descritpte the stl by name of file, title, categorie, and user we have send
    """
    title= models.CharField(max_length=30)
    NameFile = models.CharField(max_length=30)
    Description = models.CharField(max_length=300)
    Categorie = models.CharField(max_length=30)
    user = models.CharField(max_length=30)
    Com =  models.ManyToManyField(Commentaire)


