#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.contrib import messages
from django.core.files.base import ContentFile
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.shortcuts import render
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.core.files.storage import default_storage
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse
from django.core.servers.basehttp import FileWrapper
from django.template import RequestContext
from datetime import datetime
from django.contrib import messages
from .models import *
from .forms import *
from .fct import *
from werkzeug import secure_filename
import os,sys,struct, base64, time, uuid , shutil,  tempfile, zipfile
from datetime import datetime 


def home(request):
    """Renders the home page."""
    lastuser =  User.objects.order_by("-id")
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'index.html',
        context_instance = RequestContext(request,
        {
            'title':'Home',
            'username':lastuser[0],
            'year':datetime.now().year,
        })
    )

def error_404(request):
    """Renders the 404 page."""
    assert isinstance(request, HttpRequest)
    messages.error(request, 'This page does not exist than try to do O.o? ')
    return render(
        request,
        'index.html',
        context_instance = RequestContext(request,
        {
            'title':'Error 404',
            'year':datetime.now().year,
        })
    )

