"""
Definition of urls for StlStorage2.
"""

from datetime import datetime 
from django.conf.urls import patterns, url
from app.forms import *
from app.models import *
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic.edit import CreateView
from django.contrib.auth.forms import UserCreationForm
# Uncomment the next lines to enable the admin:
# from django.conf.urls import include
# from django.contrib import admin
# admin.autodiscover()
urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'app.views.home', name='home'),
    url(r'^404/$', 'app.views.error_404' , name='404'),
    url(r'^contact$', 'app.User.User.contact', name='contact'),
    url(r'^up$', 'app.User.User.upload', name='upload'),
    url(r'^contact$', 'app.User.User.contact', name='contact'),
    url(r'^view/$', 'app.Affichage.viewer.view', name='view'),
    url(r'^ImageToStl/$', 'app.Affichage.viewer.ImageToStl', name='ImageToStl'),
    url(r'^support/$', 'app.User.User.support', name='support'),
    url(r'^view/(?P<NbPage>\d+)$', 'app.Affichage.viewer.view', name='view'),
    url('^register/', CreateView.as_view(
            template_name='register.html',
            form_class=UserCreateForm,
            success_url='/'
    ), name='register'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^view/(?P<createBy>\w+)/(?P<code>\w+)', 'app.Affichage.viewer.view3d', name='3dview'),
    url(r'^view/(?P<createBy>\w+)/$', 'app.Affichage.viewer.User', name='UserList'),
    url(r'^login/$',
        'django.contrib.auth.views.login',
        {
            'template_name': 'login.html',
            'authentication_form': BootstrapAuthenticationForm,
            'extra_context':
            {
                'title':'Log in',
                'year':2015,
            }
        },
        name='login'),
    url(r'^logout$',
        'django.contrib.auth.views.logout',
        {
            'next_page': '/',
        },
        name='logout'),

    # Uncomment the admin/doc line below to enable admin documentation:
     url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls), name='admin'),
)
